package epsolution.uk.co.myrecyclerview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import epsolution.uk.co.myrecyclerview.DetailsActivity;
import epsolution.uk.co.myrecyclerview.R;
import epsolution.uk.co.myrecyclerview.model.ListItem;

/**
 * Created by Ergun Polat on 20/08/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String RATING = "rating";

    private Context mContext;
    private List<ListItem> mListItems;

    public MyAdapter(Context context, List listItem) {
        this.mContext = context;
        this.mListItems = listItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem listItem = mListItems.get(position);
        holder.mTitleTV.setText(listItem.getTitle());
        holder.mDescriptionTv.setText(listItem.getDescription());
        holder.mRating.setText(listItem.getRating());
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mTitleTV;
        TextView mDescriptionTv;
        TextView mRating;

        ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mTitleTV = (TextView) itemView.findViewById(R.id.title);
            mDescriptionTv = (TextView) itemView.findViewById(R.id.description);
            mRating = (TextView) itemView.findViewById(R.id.rating);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            ListItem item = mListItems.get(position);

            Intent detailsIntent = new Intent(mContext, DetailsActivity.class);

            detailsIntent.putExtra(NAME, item.getTitle());
            detailsIntent.putExtra(DESCRIPTION, item.getDescription());
            detailsIntent.putExtra(RATING, item.getRating());

            mContext.startActivity(detailsIntent);
        }
    }
}
