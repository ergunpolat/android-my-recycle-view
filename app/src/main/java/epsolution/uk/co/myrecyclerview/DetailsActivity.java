package epsolution.uk.co.myrecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import epsolution.uk.co.myrecyclerview.adapter.MyAdapter;

public class DetailsActivity extends AppCompatActivity {

    private TextView mNameTv, mDescriptionTv, mRatingTv;
    private Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        extras = getIntent().getExtras();

        mNameTv = (TextView) findViewById(R.id.dNameId);
        mDescriptionTv = (TextView) findViewById(R.id.dDescriptionId);
        mRatingTv = (TextView) findViewById(R.id.dRaitingId);

        if (extras != null) {

            mNameTv.setText(extras.getString(MyAdapter.NAME));
            mDescriptionTv.setText(extras.getString(MyAdapter.DESCRIPTION));
            mRatingTv.setText(extras.getString(MyAdapter.RATING));
        }


    }
}
