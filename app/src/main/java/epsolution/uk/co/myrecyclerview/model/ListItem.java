package epsolution.uk.co.myrecyclerview.model;

/**
 * Created by Ergun Polat on 20/08/2017.
 */

public class ListItem {

    private String title;
    private String description;
    private String rating;

    public ListItem(String title, String description, String rating) {
        this.title = title;
        this.description = description;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListItem listItem = (ListItem) o;

        return title != null ? title.equals(listItem.title) : listItem.title == null
                && (description != null ? description.equals(listItem.description)
                : listItem.description == null
                && (rating != null ? rating.equals(listItem.rating)
                : listItem.rating == null));

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ListItem{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }
}
